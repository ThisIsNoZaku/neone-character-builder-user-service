package io.github.thisisnozaku;

import io.github.thisisnozaku.charactercreator.NeoneCharacterCreatorUserServiceApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = NeoneCharacterCreatorUserServiceApplication.class)
@WebAppConfiguration
public class NeoneCharacterCreatorUserServiceApplicationTests {

	@Test
	public void contextLoads() {
	}

}
