package io.github.thisisnozaku.charactercreator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NeoneCharacterCreatorUserServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(NeoneCharacterCreatorUserServiceApplication.class, args);
	}
}
